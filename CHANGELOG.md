# Changelog

## aiops-agent

### 2.0.2

* [CHANGE] log-compressor: adapt to new backend endpoints (kebab-case)

### 2.0.1

* [CHANGE] log-compressor: Promtail upgrade to version 2.7.3
* [CHANGE] kube-state-metrics: upgrade to version 5.15.2

### 2.0.0

First release of new major version. 1.x.x release branch has been deprecated and is no longer operational.
