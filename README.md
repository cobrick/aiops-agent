![](logo.png)

# AIOps agent

For more information please refer to the [Official Documentation](https://observe.digital/docs/aiops-agent)

The **AIOps Agent** serves as a vital element in the successful operation of co.brick's Observe for kubernetes clusters. Its primary function is to gather and scrutinize data, which is then relayed to the central Observe system. This process ensures that co.brick's Observe can effectively accumulate and analyze data. To operate optimally, the AIOps Agent necessitates the installation of a service mesh. Currently, it is compatible with service meshes such as Linkerd, Cilium, and Dataplanev2.

The co.brick AIOps agent collects observability data and sends it to AIOps central system. Once the agent is deployed on the cluster, it collects and sends [Prometheus](https://prometheus.io/) metrics and log data using a Grafana Agent and [Promtail](https://grafana.com/docs/loki/latest/clients/promtail/) clients. Data to co.brick AIOps central system is sent using Prometheus `remote_write` [API](https://prometheus.io/docs/prometheus/latest/configuration/configuration/#remote_write).

The AIOps agent implements the idea of "Umbrella chart". 
In Helm, an umbrella chart is a packaging mechanism that facilitates the deployment of multiple Helm charts as a unified application. It acts as a container for individual service charts within a complex application. The umbrella chart includes dependencies on these service charts and allows the specification of shared configurations or values applicable to all components. When deploying the umbrella chart, Helm automatically manages the deployment of its dependent charts, ensuring a consistent and coordinated deployment of the entire application.

The full list of the subcharts can be found here: [Chart.yaml](/Chart.yaml)

## Configuration

The following tables list the parameters of the aiops agent chart and their default values.

| Parameter                           | Description                                        | Default                         |
|-------------------------------------|----------------------------------------------------|---------------------------------|
| tags.linkerd                        | Enabled Linkerd integration                        | true                            |
| tags.dataplane-v2                   | Enabled dataplane-v2 integration                   | false                           |
| tags.cilium                         | Enabled cilium integration                         | false                           |
| networkPolicy.create                | Create network policy in installed namespace       | false                           |
| grafanaAgent.globalScrapeInterval   | How frequently grafana agent should scrape metrics | 15s                             |
| global.tenant                       | Tenant name                                        | None                            |
| global.site                         | Site name (prod,stage etc.)                        | None                            |
| global.clientId                     | Client Id of the oauth2 standard                   | None                            |
| global.clientSecret                 | Client Secret of the oauth2 standard               | None                            |
| global.SecretName                   | name of the file with client oauth2                | None                            |
| global.compressionEnabled           | Either send metrics compressed or not compressed   | false                           |
| hubble-metrics-exporter.hubbleRelay | URL of deployed hubble-relay                       | aiops-agent-hubble-gke-exporter |
| hubble-metrics-exporter.hubbleRelay | PORT of deployed hubble-relay                      | 80                              |
| global.linkerdInject                | When set adds linkerd sidecar for aiops components | disabled                        |
| global.sendAllMetrics               | Wheter all metrics should be sent                  | false                           |
| node-exporter.enabled               | Wheter node-exporter should be installed           | disabled                        |

Depending on your service mesh it is required to change tags of the service mesh using values.yaml or --set flag.

## Network Policy

If enabled aiops-agent will install the deny-all policy and policy which allows metrics-compressor to collect metrics from the services.

## Deployment

Get charts from cobrick chart registry:

```bash
helm repo add cobrick https://storage.googleapis.com/public-charts
helm repo update
# View all helm charts available in registry
helm search repo cobrick
```

Deploy cobrick AIOps agent by running the following instruction:

```bash
helm upgrade --install --create-namespace aiops-agent cobrick/aiops-agent --version 2.0.1 -n aiops \
--set global.clientId={CLIENT_ID} \
--set global.clientSecret={CLIENT_SECRET} \
--set global.tenant={TENANT} \
--set global.site={SITE} \
--set tags.linkerd=true
```

**NOTE**: specifying SITE parameter is optional

It is also possible to deploy the chart by downloading it using helm pull command and then unarchive it:

```bash
helm pull cobrick/aiops-agent --version 2.x.x
tar -xvzf aiops-agent-2.x.x.tgz
helm dependency build
```

**NOTE:** default namespace in above example is aiops. If you wish to install it in different one, please use `-n` switch to change it

### Metrics Collection

By default, aiops-agent sends only the most relevant metrics for the system in order to reduce complexity and increase readability. In order to gather all metrics from the monitored system, during the aiops-agent installation add the following flag:

```bash
--set global.sendAllMetrics=true 
```

By default, aiops-agent scrapes all targets present in the system (e.g. pods). If there is a need for excluding given target from scraping process (meaning, metrics won't be captured) it must be annotated by:

```bash
prometheus.io/scrape: false
```
