#!/bin/bash
help() {
    echo "This script can be used to quickly generate 'kubectl' commands to annotate pods in the cluster for Cilium/Hubble L7 visibility."
    echo
    echo "By default it prints the generated commands to the stdout stream, but it can also execute the commands (after user confirmation) when executed with the -x option."
    echo "For scripting purposes it's possible to redirect only the resulting commands to an output file, without the 'beautifying' messages."
    echo "To do so, redirect the stderr stream to the /dev/null and then redirect the output stream to the file, e.g. ./cilium-annotate ... 2>/dev/null >output.sh"
    echo
    echo "Requirements: 'kubectl' and 'jq' commands must be available in the PATH and executable."
    echo
}

usage() {
    echo "Usage: $0 -n {namespaces} -p {ports} [options]"
    echo "where:"
    echo -e "\t{namespaces} is a comma-separated list of namespaces which should be analyzed (required)"
    echo -e "\t{ports} is a comma-separated list of ports which should be analyzed (required)"
    echo "options:"
    echo -e "\t-x Execute the commands after user confirmation"
    echo -e "\t-y When used with the -x option it skips the user input step by automatically confirming the execution (dangerous)"
    echo -e "\t-h Display help and exit"
    echo "examples:"
    echo -e "\t$0 -n app -p 80,8080 - Generate annotation commands for the pods listening on ports 80 and/or 8080 and deployed in 'app' namespace"
}

check_dependencies() {
    if ! [ -x "$(command -v kubectl)" ]; then
        echo "Error: 'kubectl' command is not available or not executable" >&2
        error=true
    fi
    if ! [ -x "$(command -v jq)" ]; then
        echo "Error: 'jq' command is not available or not executable" >&2
        error=true
    fi
    if [ "$error" = true ]; then
        return 1
    fi
}

cleanup() {
    shopt -u nocasematch
    exit 130
}

display_context() {
    echo "Connected to $(kubectl config current-context)." >&2
    echo "Analyzing namespaces: ${namespaces[@]}" >&2
}

fetch_pods() {
    echo -n "Fetching pods..." >&2
    pods=($(kubectl get pods --all-namespaces -o json 2>/dev/null | jq -c '.items[] | {name: .metadata.name, namespace: .metadata.namespace}'))
}

while getopts "hxn:p:y" opt; do
    case $opt in
        n ) 
        OIFS=$IFS
        IFS=, namespaces=($OPTARG)
        IFS=$OIFS;;
        p ) 
        OIFS=$IFS
        IFS=, allowed_ports=($OPTARG)
        IFS=$OIFS;;
        x ) execute=true;;
        y ) yes=true;;
        h ) help
        usage
        exit 0;;
        * ) usage
        exit 1;;
    esac
done

if [ -z $namespaces ] || [ -z $allowed_ports ]; then
    printf "Missing required argument\n"
    usage
    exit 1
fi

if ! check_dependencies; then
    exit 1
fi
display_context
fetch_pods
cmds=()
counter=1

for p in ${pods[@]}; do
    printf "\rAnalyzing pod $counter of ${#pods[@]}..." >&2
    let counter++
    namespace=$(echo $p | jq -r .namespace)
    ns_found=$(echo ${namespaces[@]} | grep -ow "$namespace" | wc -w)
    ns_found=$(($ns_found))
    if [[ "$ns_found" != 0 ]]; then
        name=$(echo $p | jq -r .name)
        details=$(kubectl get pod $name -n $namespace -o json 2>/dev/null | jq '{annotations: .metadata.annotations, containers: .spec.containers}')
        if [[ "$(echo $details | jq .annotations)" != *"io.cilium.proxy-visibility"* ]]; then
            values=()
            ports=($(echo $details | jq -c '.containers[].ports[] | {port: .containerPort, protocol: .protocol}' 2>/dev/null))
            for port in ${ports[@]}; do
                port_number=$(echo $port | jq -r .port)
                port_found=$(echo ${allowed_ports[@]} | grep -ow "$port_number" | wc -w)
                port_found=$(($port_found))
                if [[ "$port_found" != 0 ]]; then
                    protocol=$(echo $port | jq -r .protocol)
                    values+=("<Ingress/$port_number/$protocol/HTTP>")
                fi
            done
            if [ -n "$values" ]; then
                cmds+=("kubectl annotate pod -n $namespace $name io.cilium.proxy-visibility=\"$(IFS=,; echo -n "${values[*]}")\"")
            fi
        fi
    fi
done
printf "\r\033[2KAnalysis finished.\n" >&2
if [ -n "$cmds" ]; then
    echo
    echo "List of commands to annotate the pods:" >&2
    (IFS=$'\n'; echo "${cmds[*]}")
    if [ "$execute" = true ]; then
        echo
        if [ "$yes" = true ]; then
            REPLY="y"
        else
            read -p "Execute the commands? [y/N]: " -r
        fi
        trap cleanup INT
        shopt -s nocasematch
        if [[ $REPLY =~ ^(yes|y)$ ]]; then
            i=0
            len=${#cmds[@]}
            while [ $i -lt $len ]; do
                c=${cmds[$i]}
                let i++
                printf "\r\033[2KExecuting command $i of $len..." >&2
                eval "$c" >/dev/null
            done
            printf "\r\033[2KExecution finished.\n" >&2
        fi
        shopt -u nocasematch
    fi
else
    echo "Nothing to do here." >&2
fi
