# Linkerd deployment

**Currently, it is required to install Linkerd to use co.brick AIOPS.** If not installed co.brick AIOPS won't be able to collect [most valuable metrics](https://sre.google/sre-book/monitoring-distributed-systems/) from applications:

- Latency - The time it takes to service a request.
- Traffic -  A measure of how much demand is being placed on the system.
- Errors - The rate of requests that fails.

For meaning and reporting of these metrics from application co.brick AIOps uses [Linkerd service mesh](https://linkerd.io/). Co.brick AIOPS supports only traffic metrics collected by Linkerd.

In the future co.brick AIOps will support more service-meshes (for example Istio)

Linkerd works by installing a set of ultralight, transparent proxies next to each service instance. This design allows Linkerd and co.brick AIOps to measure and manipulate traffic to and from applications without introducing excessive latency.

The easiest way to install Linkerd is by using the CLI (command-line interface).

For more detailed instructions see official Linkerd documentation: <https://linkerd.io/2.10/getting-started/>

Data collected by Linkerd is sent to co.brick AIOPS central system using [Cobrick AIOps agent](https://bitbucket.org/cobrick/aiops-agent/src/master/). See: <https://bitbucket.org/cobrick/aiops-agent/Linkerd.md/#Integrating-cobrick-AIOps>

## Getting Started

**Before starting** it is required to have access to the Kubernetes cluster with permissions to create and edit elements and a working kubectl command on the local machine.

To install CLI manually run:

```bash
curl -sL https://run.linkerd.io/install | sh
```

The next step is to check and validate that everything is configured correctly. To check the cluster, run:

```bash
linkerd check --pre
```

If any checks do not pass, make sure to follow the provided links and fix those issues before proceeding.

After that install the Linkerd control plane - The control plane controls the data plane and provides the UI and API that can be used to configure, monitor, and operate the mesh. Run:

```bash
linkerd install | kubectl apply -f -
```

### Helm install

It is also possible to install Linkerd using [helm](https://helm.sh/). The identity component of Linkerd requires setting up a trust anchor certificate, and an issuer certificate with its key. These must use the ECDSA P-256 algorithm and need to be provided to Helm by the user (unlike when using the Linkerd install CLI which can generate these automatically).

For instructions on how to generate certificates and install Linkerd with helm see:

<https://linkerd.io/2.10/tasks/generate-certificates/>

<https://linkerd.io/2.10/tasks/install-helm/>

## Adding services to Linkerd

Adding Linkerd’s control plane to the cluster doesn’t change anything about your application. For services to take advantage of Linkerd, they need to be *meshed*, by injecting Linkerd’s data plane proxy into their pods.

This operation will cause the selected pod to redeploy, now with an additional proxy. Depending on the application it could cause some downtime and could cause [startup race conditions](https://linkerd.io/2.10/tasks/adding-your-service/#a-note-on-startup-race-conditions).

For more detailed instructions see: <https://linkerd.io/2.10/tasks/adding-your-service/>

## Meshing a service with annotations

For more stable experience we recommend meshing with resource annotation. It is also possible to [mesh using CLI.](https://bitbucket.org/cobrick/aiops-agent/Linkerd.md/#Using-CLI)

Depending on applications and their load some proxy configuration may be needed. It is done by setting additional Kubernetes annotations at the resource level before injection. See the [full list of proxy configuration options](https://linkerd.io/2.10/reference/proxy-configuration/).

### Using Annotations

Meshing a Kubernetes resource is typically done by annotating the resource, or its namespace, with the linkerd.io/inject: enabled Kubernetes annotation. This annotation triggers automatic proxy injection when the resources are created or updated. (See the [proxy injection page](https://linkerd.io/2.10/features/proxy-injection/) for more on how this works.)

For example, if all services are deployed in a single namespace it is easier to add inkerd.io/inject: enabled annotation to namespace definition. For better control inject Linkerd only into selected deployments with the same annotation. 

Example of meshed deployment:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  annotations:
    linkerd.io/inject: enabled
    meta.helm.sh/release-name: Hello-AIOps
    meta.helm.sh/release-namespace: default
  generation: 1
  labels:
    app.kubernetes.io/instance:   Hello-AIOps
    app.kubernetes.io/managed-by: Helm
...
```

### Using CLI

To inject Linkerd using CLI see the example command:

```bash
kubectl get -n default deploy -o yaml \
  | linkerd inject - \
  | kubectl apply -f -
```

This command retrieves all of the deployments running in the default namespace, runs the manifest through linkerd inject, and then reapplies it to the cluster.

## Integrating co.brick AIOps

[Cobrick AIOps agent](https://bitbucket.org/cobrick/aiops-agent/src/master/)is out of the box configured to collect both the control plane data as well as the proxy’s metrics from Linkerd using [Prometheus scrape config](https://linkerd.io/2.10/tasks/external-prometheus/#prometheus-scrape-configuration) targeting Linkerd metrics. 

For instructions on how to install and configure the Cobrick AIOps agent see: <https://bitbucket.org/cobrick/aiops-agent/src/master/README.md>