# Cilium deployment

For AIOPS to collect cilium data you need to set up Hubble Observability. It is required to enable hubble and its metrics. Example of helm flags required:

```bash
    --set prometheus.enabled=true \
    --set operator.prometheus.enabled=true \
    --set hubble.enabled=true \
    --set hubble.metrics.enabled="{dns,drop,tcp:destinationContext=pod|ip;sourceContext=pod|ip,flow:destinationContext=pod|ip;sourceContext=pod|ip,port-distribution,icmp,http:destinationContext=pod|ip;sourceContext=pod|ip}"
```

For more information see: <https://docs.cilium.io/en/stable/gettingstarted/hubble_setup/#hubble-setup> and <https://docs.cilium.io/en/stable/operations/metrics/#hubble-metrics>

Cilium in default configuration doesn�t expose source and target labels. They need to be turned on in configuration:

```bash
dns,drop,tcp:destinationContext=pod|ip;sourceContext=pod|ip,flow:destinationContext=pod|ip;sourceContext=pod|ip,port-distribution,icmp,http:destinationContext=pod|ip;sourceContext=pod|ip
```

When specifying the source and/or destination context, multiple contexts can be specified by separating them via the | symbol. When multiple are specified, then the first non-empty value is added to the metric as a label.

Hubble and its metrics will provide L3/L4 visibility (IP address identities, TCP port and flags) for all Cilium-managed traffic out of the box (e.g. via the tcp, drop and port-distribution metric), but for *L7* visibility (HTTP headers or latency, DNS response codes, etc) you need to explicitly opt-in (this applies to e.g. the http and dns metrics).

The documentation for L7 visibility annotation can be found here: <https://docs.cilium.io/en/latest/policy/visibility/>

There is a [convenience script](./examples/cilium-annotate.sh) which can be used for a quick test configuration. To see its usage syntax and available options run it with an `-h` option.
The script has some important limitations:

* it looks for the ports by analyzing the definitions of the pods, so if a pod doesn't specify any ports and relies solely on a service, it won't get annotated,
* it ignores pods which are already annotated with the `io.cilium.proxy-visibility` annotation, even if the value of the annotation is incorrect or empty,
* it generates only ingress direction annotations,
* the annotations are on pod level, so they will be removed when the pod is redeployed for whatever reason,
* it assumes all the ports are HTTP (which correctly matches HTTP/1.1, HTTP/2 and gRPC endpoints), so it can potentially generate incorrect annotations - this can potentially lead to
broken communication, e.g. in case of databases or DNS pods.

Because of the last limitation it's very important to **use the script with caution** (especially when using the `-x` option).
It is also **NOT RECOMMENDED** to use this script in any production scenario.
